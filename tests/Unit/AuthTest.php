<?php

namespace Tests\Unit;

use Tests\TestCase;
use App\Models\Users;
use App\Http\Services\AuthService;
use Illuminate\Support\Facades\Crypt;
use App\Http\Repositories\UsersRepository;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Mockery;

class AuthTest extends TestCase
{
    // use DatabaseTransactions;
    use RefreshDatabase;
    /**
     * A basic unit test example.
     *
     * @return void
     */

    public function test_success_auth_service()
    {
        $uniqueUsername = "21savage";

        $user = Users::factory()->create([
            "id" => 1,
            "username" => $uniqueUsername,
            "email" => "rizkysyahroni100@gmail.com",
            "phone" => "08871992701",
            "password" => '$2y$10$oE0dQ1k.1yHbWa6WvN2V1u9iikC7ZZx3KIT07mgzPD915FsjDoBQ6',
            "remember_token" => "eyJpdiI6Ilp2a3pCRkI5UTVyREV6WFlVR21zM0E9PSIsInZhbHVlIjoiZ0VKYkRhNXVQTW0xdWFra1k5emtTUT09IiwibWFjIjoiYWU0ZDgwZDVjMjk5YzM5ODQxNzczNWE1NDM5MjIzY2FjZmEyYjE0ZTBlMTIwNjRjNmIzYjI0NWI0NjY1MDZlMyIsInRhZyI6IiJ9",
            "created_at" => "2024-01-06 02:01:32",
            "updated_at" => "2024-01-06 02:12:22"
        ]);

        $userRepository = new UsersRepository($user);
        $authService = new AuthService($userRepository);

        Crypt::shouldReceive('encrypt')
            ->once()
            ->with($user->username)
            ->andReturn("mock_encrypt_value");

        $authServiceMock = Mockery::mock($authService);
        $authServiceMock->shouldReceive('encrypt_value')->with('encrypt_value')->andReturn("mock_encrypt_value");

        $authServiceMock->shouldReceive('table')->andReturn(true);
        $authServiceMock->shouldReceive('wasRecentlyCreated')->andReturn(true);

        $authenticate = $authService->authenticate($user->username, $user->password);

        $this->assertEquals('success', $authenticate['status']);
        // $this->assertEquals($user, $authenticate['result']);
        $this->assertEquals("mock_encrypt_value", $authenticate['encrypt_value']);
        $this->assertEquals(86400, $authenticate['time']);

        Mockery::close();
    }
}
