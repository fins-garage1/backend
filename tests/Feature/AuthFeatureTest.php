<?php

namespace Tests\Feature;

use App\Models\Users;
use Carbon\Carbon;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\DB;
use Tests\TestCase;

/**
 * ClassNameTest
 * @group group
 */
class AuthFeatureTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function test_authenticate_api_success()
    {
        $staticTimestamp = Carbon::create(2023, 12, 31, 7, 7, 41);

        DB::shouldReceive('users')->andReturnSelf();

        Users::create([
            "username" => "escurtcheon123",
            "email" => "rizkysyahroni200@gmail.com",
            "phone" => "08871992701",
            "password" => '$2y$04$KB1DP64R98HdrSUHy1qJkOfrI3EKCELt6LYfnlaA73T0T4pRhP76K',
            "remember_token" => "#################",
            'created_at' => $staticTimestamp,
            'updated_at' => $staticTimestamp,
        ]);

        $payload = ['username' => 'escurtcheon123', 'password' => 'kaneki123', 'api' => true];

        $response = $this->post('/auth/login-submit', $payload);

        $response->assertStatus(200);

        $response->assertJson([
            "status" => "success",
            "result" => [
                "id" => 1,
                "username" => "escurtcheon123",
                "email" => "rizkysyahroni200@gmail.com",
                "phone" => "08871992701",
                "password" => '$2y$04$KB1DP64R98HdrSUHy1qJkOfrI3EKCELt6LYfnlaA73T0T4pRhP76K',
                "remember_token" => "#################",
            ]
        ]);
    }

    public function test_authenticate_api_username_and_password_failed()
    {
        $staticTimestamp = Carbon::create(2023, 12, 31, 7, 7, 41);

        DB::shouldReceive('users')->andReturnSelf();

        Users::create([
            "username" => "escurtcheon123",
            "email" => "rizkysyahroni200@gmail.com",
            "phone" => "08871992701",
            "password" => '$2y$04$KB1DP64R98HdrSUHy1qJkOfrI3EKCELt6LYfnlaA73T0T4pRhP76K',
            "remember_token" => "#################",
            'created_at' => $staticTimestamp,
            'updated_at' => $staticTimestamp,
        ]);

        $payload = ['username' => 'escurtcheon1234', 'password' => '123123', 'api' => true];

        $response = $this->post('/auth/login-submit', $payload);

        $response->assertStatus(400);

        $response->assertJson([
            "message" => "Username or password is wrong"
        ]);
    }

    public function test_authenticate_views_success()
    {
        $staticTimestamp = Carbon::create(2023, 12, 31, 7, 7, 41);

        DB::shouldReceive('users')->andReturnSelf();

        Users::create([
            "username" => "escurtcheon123",
            "email" => "rizkysyahroni200@gmail.com",
            "phone" => "08871992701",
            "password" => '$2y$04$KB1DP64R98HdrSUHy1qJkOfrI3EKCELt6LYfnlaA73T0T4pRhP76K',
            "remember_token" => "#################",
            'created_at' => $staticTimestamp,
            'updated_at' => $staticTimestamp,
        ]);

        $payload = ['username' => 'escurtcheon123', 'password' => 'kaneki123', 'api' => false];

        $response = $this->post('/auth/login-submit', $payload);

        $response->assertStatus(302)
            ->assertRedirect(route('dashboard-general-dashboard'));

        $response->assertCookie('login');
    }

    public function test_authenticate_views_failed()
    {
        $staticTimestamp = Carbon::create(2023, 12, 31, 7, 7, 41);

        DB::shouldReceive('users')->andReturnSelf();

        Users::create([
            "username" => "escurtcheon123",
            "email" => "rizkysyahroni200@gmail.com",
            "phone" => "08871992701",
            "password" => '$2y$04$KB1DP64R98HdrSUHy1qJkOfrI3EKCELt6LYfnlaA73T0T4pRhP76K',
            "remember_token" => "#################",
            'created_at' => $staticTimestamp,
            'updated_at' => $staticTimestamp,
        ]);

        $payload = ['username' => 'escurtcheon1235', 'password' => 'kaneki123', 'api' => false];

        $response = $this->post('/auth/login-submit', $payload);

        $response->assertStatus(302)
            ->assertRedirect('http://localhost');
    }
}
