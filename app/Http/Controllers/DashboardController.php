<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Interfaces\Controllers\IDashboardController;
use App\Interfaces\Services\IDashboardService;

class DashboardController extends Controller implements IDashboardController
{
    private $dashboardService;

    public function __construct(IDashboardService $dashboardService)
    {
        $this->dashboardService = $dashboardService;
    }

    public function page(Request $request)
    {
        $periode = $request->query('periode', 'day');
        $response = $this->dashboardService->page($periode);

        return view('pages.dashboard-general-dashboard', ['type_menu' => 'dashboard', 'result' => $response]);
    }
}
