<?php

namespace App\Http\Controllers;

use App\Builder\UsersBuilder;
use Illuminate\Http\Request;
use App\Requests\AuthRequest;
use App\Interfaces\Controllers\IAuthController;
use App\Interfaces\Services\IAuthService;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller implements IAuthController
{
    private $authService;
    private $authRequest;

    public function __construct(IAuthService $authService, AuthRequest $authRequest)
    {
        $this->authService = $authService;
        $this->authRequest = $authRequest;
    }

    public function authenticate(Request $request)
    {
        $api = $request->input('api', false);

        ['username' => $username, 'password' => $password] = $request->validate($this->authRequest->rules()[0]);

        $response = $this->authService->authenticate($username, $password);

        $request->session()->put('login', $response['encrypt_value'], $response['time']);

        if ($api) {
            return  $response = response()->json(["status" => $response['status'], 'result' => $response['result']], 200)->withCookie('login', $response['encrypt_value'], $response['time']);
        }

        return redirect()->route('dashboard-general-dashboard')->with('authResponse', $response)->cookie('login', $response['encrypt_value'], $response['time']);
    }

    public function register(Request $request)
    {
        ['username' => $username, 'email' => $email, 'phone' => $phone, 'password' => $password] = $request->validate($this->authRequest->rules()[1]);

        $usersDto = (new UsersBuilder())
            ->setUsername($username)
            ->setEmail($email)
            ->setPhone($phone)
            ->setPassword($password)
            ->build();

        $response = $this->authService->register($usersDto);

        return response()->json($response, 200);
    }

    public function logout(Request $request)
    {
        Auth::logout();

        $request->session()->flush();
        $request->session()->invalidate();

        return redirect()->route('auth-login-page')->cookie('login', null, -1);
    }

    public function getCookie(Request $request)
    {
        $result = $request->cookie('login');

        return response()->json(['result' => $result], 200);
    }
}
