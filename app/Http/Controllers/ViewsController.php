<?php

namespace App\Http\Controllers;

use App\Interfaces\Controllers\IViewsController;
use App\Interfaces\Services\IViewsService;
use App\Requests\ViewsRequest;
use Illuminate\Http\Request;

class ViewsController extends Controller implements IViewsController
{
    private $viewsService;
    private $viewsRequest;

    public function __construct(IViewsService $viewsService, ViewsRequest $viewsRequest)
    {
        $this->viewsService = $viewsService;
        $this->viewsRequest = $viewsRequest;
    }

    public function get(Request $request)
    {
        $api = $request->query('api', false);
        $response = $this->viewsService->get(
            $request->query('filter', 'day')
        );

        if ($api) {
            return response()->json($response, 200);
        }

        return redirect()->back()->with('get_data_view', $response);
    }

    public function create(Request $request)
    {
        $api = $request->input('api', false);
        ['ip_address' => $ip_address] = $request->validate($this->viewsRequest->rules()[0]);

        $response = $this->viewsService->create(
            $ip_address,
        );

        if ($api) {
            return response()->json($response, 200);
        }

        return redirect()->back()->with('create_view', $response);
    }

    public function update(Request $request, $id)
    {
        $api = $request->input('api', false);
        ['ip_address' => $ip_address] = $request->validate($this->viewsRequest->rules()[0]);

        $response = $this->viewsService->update($id, $ip_address);

        if ($api) {
            return response()->json($response, 200);
        }

        return redirect()->back()->with('update_view', $response);
    }

    public function delete(Request $request, $id)
    {
        $api = $request->query('api', false);

        $response = $this->viewsService->delete((int)$id);

        if ($api) {
            return response()->json($response, 200);
        }

        return redirect()->back()->with('result_by_id', $response);
    }

    public function getById(Request $request, $id)
    {
        $api = $request->query('api', false);
        $response = $this->viewsService->getById($id);

        if ($api) {
            return response()->json($response, 200);
        }

        return redirect()->back()->with('view_by_id', $response);
    }
}
