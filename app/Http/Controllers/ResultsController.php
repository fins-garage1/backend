<?php

namespace App\Http\Controllers;

use App\Builder\ResultsBuilder;
use Illuminate\Http\Request;
use App\Requests\ResultsRequest;
use App\Interfaces\Controllers\IResultsController;
use App\Interfaces\Services\IResultsService;

class ResultsController extends Controller implements IResultsController
{
    private $resultsService;
    private $resultsRequest;

    public function __construct(IResultsService $resultsService, ResultsRequest $resultsRequest)
    {
        $this->resultsService = $resultsService;
        $this->resultsRequest = $resultsRequest;
    }

    public function get(Request $request)
    {
        $api = $request->query('api');

        $response = $this->resultsService->get(
            $request->query('limit', 5),
            $request->query('order_by', 'id'),
            $request->query('sort', 'ASC')
        );

        if ($api) {
            return response()->json($response, 200);
        }

        return redirect()->back()->with('result', $response);
    }

    public function create(Request $request)
    {
        $api = $request->input('api', false);
        ['name' => $name, 'total' => $total] = $request->validate($this->resultsRequest->rules()[0]);

        $resultsDto = (new ResultsBuilder())
            ->setName($name)
            ->setTotal($total)
            ->build();

        $response = $this->resultsService->create($resultsDto);

        if ($api) {
            return response()->json($response, 200);
        }

        return redirect()->back()->with('result', $response);
    }

    public function update(Request $request, $id)
    {
        $api = $request->input('api', false);
        ['name' => $name, 'total' => $total] = $request->validate($this->resultsRequest->rules()[0]);

        $resultsDto = (new ResultsBuilder())
            ->setName($name)
            ->setTotal($total)
            ->build();

        $response = $this->resultsService->update($id, $resultsDto);

        if ($api) {
            return response()->json($response, 200);
        }

        return redirect()->back()->with('result', $response);
    }

    public function delete(Request $request, $id)
    {
        $api = $request->query('api', false);
        $response = $this->resultsService->delete((int)$id);

        if ($api) {
            return response()->json($response, 200);
        }

        return redirect()->back()->with('result', $response);
    }

    public function getById(Request $request, $id)
    {
        $api = $request->query('api', false);
        $response = $this->resultsService->getById($id);

        if ($api) {
            return response()->json($response, 200);
        }

        return redirect()->back()->with('result_by_id', $response);
    }

    public function page(Request $request)
    {
        $search = $request->query('search', false);
        $response = $this->resultsService->page($search);


        return view('pages.our-results-page', ['type_menu' => 'result', 'result' => $response]);
    }
}
