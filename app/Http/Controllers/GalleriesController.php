<?php

namespace App\Http\Controllers;

use App\Requests\GalleriesRequest;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Interfaces\Controllers\IGalleriesController;
use App\Interfaces\Services\IGalleriesService;

class GalleriesController extends Controller implements IGalleriesController
{
    private $galleriesService;
    private $galleriesRequest;

    public function __construct(IGalleriesService $galleriesService, GalleriesRequest $galleriesRequest)
    {
        $this->galleriesService = $galleriesService;
        $this->galleriesRequest = $galleriesRequest;
    }

    public function get(Request $request)
    {
        $api = $request->query('api', false);

        $response = $this->galleriesService->get(
            $request->query('limit', 5),
            $request->query('order_by', 'id'),
            $request->query('sort', 'asc')
        );

        if ($api) {
            return response()->json($response, 200);
        }

        return redirect()->back()->with('get_data_testimoni', $response);
    }

    public function getById(Request $request, $id)
    {
        $api = $request->query('api', false);

        $response = $this->galleriesService->getById($id);

        if ($api) {
            return response()->json($response, 200);
        }

        return redirect()->back()->with('galleri_by_id', $response);
    }

    public function create(Request $request)
    {
        $image = $request->file('image');
        $api = $request->input('api', false);

        $request->validate($this->galleriesRequest->rules()[0]);

        $image_name = 'image_gallery' . '-' . Str::uuid() . '.' . $image->getClientOriginalExtension();
        $image->storePubliclyAs("galleries", $image_name, "public");

        $response = $this->galleriesService->create($image_name);
        if ($api) {
            return response()->json($response, 200);
        }

        return redirect()->back()->with('result', $response);
    }

    public function update(Request $request, $id)
    {
        $image = $request->file('image');
        $api = $request->input('api', false);

        $request->validate($this->galleriesRequest->rules()[0]);

        $response = $this->galleriesService->update((int)$id, $image);
        if ($api) {
            return response()->json($response, 200);
        }

        return redirect()->back()->with('result', $response);
    }

    public function delete(Request $request, $id)
    {
        $api = $request->query('api', false);
        $response = $this->galleriesService->delete((int)$id);

        if ($api) {
            return response()->json($response, 200);
        }

        return redirect()->back()->with('result', $response);
    }

    public function page()
    {
        $response =  $this->galleriesService->page();

        return view('pages.galeri-page', ['type_menu' => 'gallery', 'result' => $response]);
    }
}
