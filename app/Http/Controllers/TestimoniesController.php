<?php

namespace App\Http\Controllers;

use App\Builder\TestimoniesBuilder;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Requests\TestimoniesRequest;
use App\Interfaces\Controllers\ITestimoniesController;
use App\Interfaces\Services\ITestimoniesService;

class TestimoniesController extends Controller implements ITestimoniesController
{
    private $testimoniesService;
    private $testimoniesRequest;

    public function __construct(ITestimoniesService $testimoniesService, TestimoniesRequest $testimoniesRequest)
    {
        $this->testimoniesService = $testimoniesService;
        $this->testimoniesRequest = $testimoniesRequest;
    }

    public function get(Request $request)
    {
        $api = $request->query('api', false);

        $response = $this->testimoniesService->get(
            $request->query('limit', 5),
            $request->query('order_by', 'id'),
            $request->query('sort', 'ASC')
        );

        if ($api) {
            return response()->json($response, 200);
        }

        return redirect()->back()->with('get_data_testimoni', $response);
    }

    public function create(Request $request)
    {
        $image = $request->file('image');
        $api = $request->input('api', false);
        ['name' => $name, 'position' => $position, 'feedback' => $feedback] = $request->validate($this->testimoniesRequest->rules()[0]);

        $image_name = 'image_testimoni' . '-' . Str::uuid() . '.' . $image->getClientOriginalExtension();

        $image->storePubliclyAs("testimonies", $image_name, "public");

        $testimoniesDto = (new TestimoniesBuilder())
            ->setImage($image_name)
            ->setName($name)
            ->setPosition($position)
            ->setFeedback($feedback)
            ->build();

        $response = $this->testimoniesService->create($testimoniesDto);

        if ($api) {
            return response()->json($response, 200);
        }

        return redirect()->back()->with('create_testimoni', $response);
    }

    public function update(Request $request, $id)
    {
        $image = $request->file('image');
        $api = $request->input('api', false);
        ['name' => $name, 'position' => $position, 'feedback' => $feedback] = $request->validate($this->testimoniesRequest->rules()[0]);

        if (empty(!$image)) {
            $image_name = 'image_testimoni' . '-' . Str::uuid() . '.' . $image->getClientOriginalExtension();
            $image->storePubliclyAs("testimonies", $image_name, "public");
        }

        $testimoniesDto = (new TestimoniesBuilder())
            ->setImage($image_name)
            ->setName($name)
            ->setPosition($position)
            ->setFeedback($feedback)
            ->build();

        $response = $this->testimoniesService->update($id, $testimoniesDto);

        if ($api) {
            return response()->json($response, 200);
        }

        return redirect()->back()->with('update_testimoni', $response);
    }

    public function delete(Request $request, $id)
    {
        $api = $request->query('api', false);
        $response = $this->testimoniesService->delete((int)$id);

        if ($api) {
            return response()->json($response, 200);
        }

        return redirect()->back()->with('result_by_id', $response);
    }

    public function getById(Request $request, $id)
    {
        $api = $request->query('api', false);
        $response = $this->testimoniesService->getById($id);

        if ($api) {
            return response()->json($response, 200);
        }

        return redirect()->back()->with('testimoni_by_id', $response);
    }

    public function page(Request $request)
    {
        $search = $request->query('search', false);
        $response = $this->testimoniesService->page(5, 'id', 'asc', $search);

        return view('pages.testimoni-page', ['type_menu' => 'testimoni', 'result' => $response]);
    }
}
