<?php

namespace App\Http\Controllers;

use App\Builder\IntroductionBuilder;
use Illuminate\Http\Request;
use App\Requests\IntroductionRequest;
use App\Interfaces\Controllers\IIntroductionController;
use App\Interfaces\Services\IIntroductionService;

class IntroductionController extends Controller implements IIntroductionController
{
    private $introductionService;
    private $introductionRequest;

    public function __construct(IIntroductionService $introductionService, IntroductionRequest $introductionRequest)
    {
        $this->introductionService = $introductionService;
        $this->introductionRequest = $introductionRequest;
    }

    public function get(Request $request)
    {
        $api = $request->query('api');

        $response = $this->introductionService->get();

        if ($api) {
            return response()->json($response, 200);
        }

        return redirect()->back()->with('result', $response);
    }

    public function create(Request $request)
    {
        $api = $request->input('api', false);
        ['paragraph' => $paragraph, 'video' => $video] = $request->validate($this->introductionRequest->rules()[0]);

        $introductionDto = (new IntroductionBuilder())
            ->setParagraph($paragraph)
            ->setVideo($video)
            ->build();

        $response = $this->introductionService->create($introductionDto);

        if ($api) {
            return response()->json($response, 200);
        }
    }

    public function update(Request $request)
    {
        $api = $request->input('api', false);
        ['paragraph' => $paragraph, 'video' => $video] = $request->validate($this->introductionRequest->rules()[0]);

        $introductionDto = (new IntroductionBuilder())
            ->setParagraph($paragraph)
            ->setVideo($video)
            ->build();

        $response = $this->introductionService->update($introductionDto);

        if ($api) {
            return response()->json($response, 200);
        } else {
            return redirect()->back()->with('response', $response);
        }
    }

    public function page()
    {
        $response = $this->introductionService->page();

        return view('pages.perkenalan-page', ['type_menu' => 'introduction', 'result' => $response]);
    }
}
