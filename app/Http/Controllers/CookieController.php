<?php

namespace App\Http\Controllers;

use App\Interfaces\Controllers\ICookieController;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class CookieController extends Controller implements ICookieController
{
    public function setCookie()
    {
        $response = response('Hello Cookie');

        $response->cookie('cookie_consent', Str::uuid(), 1000);

        return $response;
    }

    public function getCookie(Request $request)
    {
        $result = $request->cookie('cookie_consent');

        return response()->json(['result' => $result], 200);
    }

    public function delCookie()
    {
        return response('deleted')->cookie('cookie_consent', null, -1);
    }
}
