<?php

namespace App\Http\Controllers;

use App\Builder\UsersBuilder;
use Illuminate\Http\Request;
use App\Requests\UsersRequest;
use App\Interfaces\Controllers\IUsersController;
use App\Interfaces\Services\IUsersService;

class UsersController extends Controller implements IUsersController
{
    private $usersService;
    private $usersRequest;

    public function __construct(IUsersService $usersService, UsersRequest $usersRequest)
    {
        $this->usersService = $usersService;
        $this->usersRequest = $usersRequest;
    }

    public function get(Request $request)
    {
        $response = $this->usersService->get(
            $request->query('limit'),
            $request->query('order_by'),
            $request->query('sort')
        );

        return response()->json($response, 200);
    }

    public function create(Request $request)
    {
        ['username' => $username, 'email' => $email, 'phone' => $phone, 'password' => $password, 'remember_token' => $remember_token] = $request->validate($this->usersRequest->rules()[0]);

        $usersDto = (new UsersBuilder())
            ->setUsername($username)
            ->setEmail($email)
            ->setPhone($phone)
            ->setPassword($password)
            ->setRememberToken($remember_token)
            ->build();

        $response = $this->usersService->create($usersDto);
        return response()->json($response, 200);
    }

    public function update(Request $request, $id)
    {

        ['username' => $username, 'email' => $email, 'phone' => $phone, 'password' => $password, 'remember_token' => $remember_token] = $request->validate($this->usersRequest->rules()[0]);

        $usersDto = (new UsersBuilder())
            ->setUsername($username)
            ->setEmail($email)
            ->setPhone($phone)
            ->setPassword($password)
            ->setRememberToken($remember_token)
            ->build();

        $response = $this->usersService->update((int)$id, $usersDto);
        return response()->json($response, 200);
    }

    public function delete($id)
    {
        $response = $this->usersService->delete((int)$id);
        return response()->json($response, 200);
    }
}
