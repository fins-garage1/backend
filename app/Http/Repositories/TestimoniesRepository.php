<?php

namespace App\Http\Repositories;

use App\Interfaces\Repositories\ITestimoniesRepository;
use App\Models\Testimonies;

class TestimoniesRepository implements ITestimoniesRepository
{
    protected $testimoniesModels;

    public function __construct(Testimonies $testimoniesModels)
    {
        $this->testimoniesModels = $testimoniesModels;
    }

    public function get($limit, $order_by = 'id', $sort = 'asc', $search = '')
    {
        $query = $this->testimoniesModels::query();

        if ($search) {
            $query->where(function ($q) use ($search) {
                $q->where('name', 'like', '%' . $search . '%')
                    ->orWhere('position', 'like', '%' . $search . '%')
                    ->orWhere('feedback', 'like', '%' . $search . '%');
            });
        }

        // Apply pagination before ordering
        $rows = $query->orderBy($order_by, $sort)
            ->paginate($limit);

        return $rows;
    }

    public function getById($id)
    {
        $rows = $this->testimoniesModels::find($id);

        return $rows;
    }


    public function getTotalCount()
    {
        $rows = $this->testimoniesModels::count();

        return $rows;
    }

    public function getByKeys($keys, $value)
    {
        if ($value) {
            $rows = $this->testimoniesModels::where($keys, $value)->first();
        } else {
            $rows = $this->testimoniesModels::where($keys)->get();
        }

        return $rows;
    }

    public function create($data)
    {
        $rows = $this->testimoniesModels::create($data);

        return $rows;
    }

    public function update($id, $data)
    {
        $rows = $this->testimoniesModels::where('id', $id)->update($data);

        return $rows;
    }

    public function delete($id)
    {
        $rows = $this->testimoniesModels::where('id', $id)->delete();

        return $rows;
    }
}
