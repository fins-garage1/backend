<?php

namespace App\Http\Repositories;

use App\Interfaces\Repositories\IIntroductionRepository;
use App\Models\Introduction;

class IntroductionRepository implements IIntroductionRepository
{
    protected $introductionModels;

    public function __construct(Introduction $introductionModels)
    {
        $this->introductionModels = $introductionModels;
    }

    public function getByKeys($keys, $value)
    {
        if ($value) {
            $rows = $this->introductionModels::where($keys, $value)->first();
        } else {
            $rows = $this->introductionModels::where($keys, $value)->first();
        }

        return $rows;
    }

    public function get()
    {
        $rows = $this->introductionModels::orderBy('id')->get();

        return $rows;
    }

    public function create($data)
    {
        $rows = $this->introductionModels::create($data);

        return $rows;
    }

    public function update($id, $data)
    {
        $row = $this->introductionModels::find($id);

        if (!$row) {
            return false;
        }

        return $row->update($data);
    }
}
