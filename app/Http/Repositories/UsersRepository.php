<?php
   namespace App\Http\Repositories;

use App\Interfaces\Repositories\IUsersRepository;
use App\Models\Users;

   class UsersRepository implements IUsersRepository {
        protected $userModels;

        public function __construct(Users $userModels) {
            $this->userModels = $userModels;
        }

        public function get($limit = 5, $order_by = 'id', $sort = 'asc') {
            $rows = $this->userModels::orderBy($order_by, $sort)->limit($limit)->get();

            return $rows;
        }

        public function getTotalCount()
        {
            $rows = $this->userModels::count();

            return $rows;
        }

         public function getByKeys($keys, $value) {
            if($value) {
                $rows = $this->userModels::where($keys, $value)->first();
            } else {
                $rows = $this->userModels::where($keys)->get();
            }

            return $rows;
         }

         public function create($data) {
                $rows = $this->userModels::create($data);

                return $rows;
         }

        public function update($id , $data) {
            $rows = $this->userModels::where('id', $id)->update($data);

            return $rows;
        }

        public function delete($id) {
            $rows = $this->userModels::where('id', $id)->delete();

            return $rows;
        }
   }
