<?php

namespace App\Http\Repositories;

use App\Interfaces\Repositories\IResultsRepository;
use App\Models\Results;


class ResultsRepository implements IResultsRepository
{
    protected $resultsRepository;

    public function __construct(Results $resultsRepository)
    {
        $this->resultsRepository = $resultsRepository;
    }

    public function get($limit = 5, $order_by = 'id', $sort = 'asc', $search = '')
    {
        $query = $this->resultsRepository::query();

        if ($search) {
            $query->where(function ($q) use ($search) {
                $q->where('name', 'like', '%' . $search . '%')
                    ->orWhere('total', 'like', '%' . $search . '%');
            });
        }

        $rows = $query->orderBy($order_by, $sort)
            ->paginate($limit);

        return $rows;
    }

    public function getById($id)
    {
        $rows = $this->resultsRepository::find($id);

        return $rows;
    }

    public function getTotalCount()
    {
        $rows = $this->resultsRepository::count();

        return $rows;
    }

    public function getByKeys($keys, $value)
    {
        if ($value) {
            $rows = $this->resultsRepository::where($keys, $value)->first();
        } else {
            $rows = $this->resultsRepository::where($keys)->get();
        }

        return $rows;
    }

    public function create($data)
    {
        $rows = $this->resultsRepository::create($data);

        return $rows;
    }

    public function update($id, $data)
    {
        $rows = $this->resultsRepository::where('id', $id)->update($data);

        return $rows;
    }

    public function delete($id)
    {
        $rows = $this->resultsRepository::where('id', $id)->delete();

        return $rows;
    }
}
