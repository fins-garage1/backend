<?php

namespace App\Http\Repositories;

use App\Interfaces\Repositories\IGalleriesRepository;
use App\Models\Galleries;

class GalleriesRepository implements IGalleriesRepository
{
    protected $galleriesModels;

    public function __construct(Galleries $galleriesModels)
    {
        $this->galleriesModels = $galleriesModels;
    }

    public function get($limit = 5, $order_by = 'id', $sort = 'asc')
    {
        $query = $this->galleriesModels::query();

        $rows = $query->orderBy($order_by, $sort)
            ->paginate($limit);

        return $rows;
    }

    public function getById($id)
    {
        $rows = $this->galleriesModels::find($id);

        return $rows;
    }

    public function getTotalCount()
    {
        $rows = $this->galleriesModels::count();

        return $rows;
    }

    public function getByKeys($keys, $value)
    {
        if ($value) {
            $rows = $this->galleriesModels::where($keys, $value)->first();
        } else {
            $rows = $this->galleriesModels::where($keys)->get();
        }

        return $rows;
    }

    public function create($data)
    {
        $rows = $this->galleriesModels::create($data);

        return $rows;
    }

    public function update($id, $data)
    {
        $rows = $this->galleriesModels::where('id', $id)->update($data);

        return $rows;
    }

    public function delete($id)
    {
        $rows = $this->galleriesModels::where('id', $id)->delete();

        return $rows;
    }
}
