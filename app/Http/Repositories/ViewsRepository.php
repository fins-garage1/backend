<?php

namespace App\Http\Repositories;

use App\Interfaces\Repositories\IViewsRepository;
use App\Models\Views;
use Illuminate\Support\Facades\DB;

class ViewsRepository implements IViewsRepository
{
    protected $viewsModels;

    public function __construct(Views $viewsModels)
    {
        $this->viewsModels = $viewsModels;
    }

    public function get($filter)
    {
        switch ($filter) {
            case 'year':
                return $this->getData($filter, 'CURDATE() - INTERVAL 7 YEAR', 'YEAR');
                break;
            case 'month':
                return $this->getData($filter, 'CURDATE() - INTERVAL 12 MONTH', 'MONTH');
                break;
            default:
                return $this->getData($filter, 'CURDATE() - INTERVAL 7 DAY', 'DAY');
        }
    }

    protected function getData($filter, $startDate, $groupBy)
    {
        return DB::table('views')
            ->select(
                DB::raw("MAX(created_at) as created_at"),
                DB::raw("$groupBy(created_at) as $filter"),
                DB::raw('COUNT(*) as total')
            )
            ->where('created_at', '>=', DB::raw($startDate))
            ->groupBy(DB::raw("$groupBy(created_at)"))
            ->get();
    }

    public function getById($id)
    {
        $rows = $this->viewsModels::find($id);

        return $rows;
    }

    public function getTotalCount()
    {
        $rows = $this->viewsModels::count();

        return $rows;
    }

    public function getByKeys($keys, $value)
    {
        if ($value) {
            $rows = $this->viewsModels::where($keys, $value)->first();
        } else {
            $rows = $this->viewsModels::where($keys)->get();
        }

        return $rows;
    }

    public function create($data)
    {
        $rows = $this->viewsModels::create($data);

        return $rows;
    }

    public function update($id, $data)
    {
        $rows = $this->viewsModels::where('id', $id)->update($data);

        return $rows;
    }

    public function delete($id)
    {
        $rows = $this->viewsModels::where('id', $id)->delete();

        return $rows;
    }
}
