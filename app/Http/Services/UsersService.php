<?php

namespace App\Http\Services;

use App\Dto\UsersDto;
use App\Interfaces\Repositories\IUsersRepository;
use App\Interfaces\Services\IUsersService;

class UsersService implements IUsersService
{
    protected $usersRepository;

    public function __construct(IUsersRepository $usersRepository)
    {
        $this->usersRepository = $usersRepository;
    }

    public function get(int $limit, string $order_by, string $sort)
    {

        $data = $this->usersRepository->get($limit, $order_by, $sort);
        $data_count = $this->usersRepository->getTotalCount();

        return ['status' => 'success', 'result' => ['data' => $data, 'count' => $data_count]];
    }

    public function create(UsersDto $usersDto)
    {
        $result = $this->usersRepository->create((array)$usersDto);

        return ['status' => 'success', 'result' => $result];
    }

    public function update(int $id, UsersDto $usersDto)
    {

        $result = $this->usersRepository->update($id, (array)$usersDto);

        return ['status' => 'success', 'result' => $result];
    }

    public function delete(int $id)
    {
        $result = $this->usersRepository->delete($id);

        return ['status' => 'success', 'result' => $result];
    }
}
