<?php

namespace App\Http\Services;

use Illuminate\Support\Str;
use App\Interfaces\Repositories\IGalleriesRepository;
use App\Interfaces\Services\IGalleriesService;

class GalleriesService implements  IGalleriesService
{
    protected $galleriesRepository;

    public function __construct(IGalleriesRepository $galleriesRepository)
    {
        $this->galleriesRepository = $galleriesRepository;
    }

    public function get(int $limit, string $order_by, string $sort)
    {
        $data = $this->galleriesRepository->get($limit, $order_by, $sort);
        $data_count = $this->galleriesRepository->getTotalCount();

        return ['status' => 'success', 'result' => ['data' => $data, 'count' => $data_count]];
    }

    public function getById(int $id)
    {
        $data = $this->galleriesRepository->getById($id);

        return ['status' => 'success', 'result' => ['data' => $data]];
    }


    public function create(string $image)
    {
        $result = $this->galleriesRepository->create(array('image' => $image));

        return ['status' => 'success', 'result' => $result];
    }


    public function update(int $id, $image)
    {
        $data = [];
        if (empty(!$image)) {
            $image_name = 'image_gallery' . '-' . Str::uuid() . '.' . $image->getClientOriginalExtension();
            $image->storePubliclyAs("galleries", $image_name, "public");

            $data['image'] = $image_name;
        }

        $result = $this->galleriesRepository->update($id, $data);

        return ['status' => 'success', 'result' => $result];
    }

    public function delete(int $id)
    {
        $result = $this->galleriesRepository->delete($id);

        return ['status' => 'success', 'result' => $result];
    }

    public function page()
    {
        $data = $this->galleriesRepository->get();

        return $data;
    }
}
