<?php

namespace App\Http\Services;

use App\Interfaces\Repositories\IViewsRepository;
use App\Interfaces\Services\IViewsService;

class ViewsService implements IViewsService
{
    protected $viewsRepository;

    public function __construct(IViewsRepository $viewsRepository)
    {
        $this->viewsRepository = $viewsRepository;
    }

    public function get(string $filter)
    {
        $data = $this->viewsRepository->get($filter);

        return ['status' => 'success', 'result' => $data];
    }

    public function getById(int $id)
    {
        $data = $this->viewsRepository->getById($id);

        return ['status' => 'success', 'result' => ['data' => $data]];
    }


    public function create(string $ip_address)
    {
        $result = $this->viewsRepository->create(array('ip_address' => $ip_address));

        return ['status' => 'success', 'result' => $result];
    }


    public function update(int $id, string $ip_address)
    {
        $data = ['ip_address' => $ip_address];

        $result = $this->viewsRepository->update($id, $data);

        return ['status' => 'success', 'result' => $result];
    }

    public function delete(int $id)
    {
        $result = $this->viewsRepository->delete($id);

        return ['status' => 'success', 'result' => $result];
    }

    public function page(string $search)
    {

        $data = $this->viewsRepository->get($search);

        return $data;
    }
}
