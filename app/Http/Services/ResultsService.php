<?php

namespace App\Http\Services;

use App\Dto\ResultsDto;
use App\Interfaces\Repositories\IResultsRepository;
use App\Interfaces\Services\IResultsService;

class ResultsService implements IResultsService
{
    protected $resultsRepository;

    public function __construct(IResultsRepository $resultsRepository)
    {
        $this->resultsRepository = $resultsRepository;
    }

    public function get(int $limit, string $order_by, string $sort)
    {
        $data = $this->resultsRepository->get($limit, $order_by, $sort);
        $data_count = $this->resultsRepository->getTotalCount();

        return ['status' => 'success', 'result' => ['data' => $data, 'count' => $data_count]];
    }

    public function getById(int $id)
    {
        $data = $this->resultsRepository->getById($id);

        return ['status' => 'success', 'result' => ['data' => $data]];
    }

    public function create(ResultsDto $resultsDto)
    {
        $result = $this->resultsRepository->create((array)$resultsDto);

        return ['status' => 'success', 'result' => $result];
    }

    public function update(int $id, ResultsDto $resultsDto)
    {
        $result = $this->resultsRepository->update($id, (array)$resultsDto);

        return ['status' => 'success', 'result' => $result];
    }

    public function delete(int $id)
    {
        $result = $this->resultsRepository->delete($id);

        return ['status' => 'success', 'result' => $result];
    }

    public function page(string $search)
    {
        $data = $this->resultsRepository->get($search);

        return $data;
    }
}
