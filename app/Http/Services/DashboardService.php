<?php

namespace App\Http\Services;

use App\Interfaces\Repositories\IGalleriesRepository;
use App\Interfaces\Repositories\IResultsRepository;
use App\Interfaces\Repositories\ITestimoniesRepository;
use App\Interfaces\Repositories\IViewsRepository;
use App\Interfaces\Services\IDashboardService;

class DashboardService implements IDashboardService
{
    private $galleriesRepository;
    private $testimoniesRepository;
    private $resultsRepository;
    private $viewsRepository;

    public function __construct(IGalleriesRepository $galleriesRepository, ITestimoniesRepository $testimoniesRepository, IResultsRepository $resultsRepository, IViewsRepository $viewsRepository)
    {
        $this->galleriesRepository = $galleriesRepository;
        $this->testimoniesRepository = $testimoniesRepository;
        $this->resultsRepository = $resultsRepository;
        $this->viewsRepository = $viewsRepository;
    }

    public function page(string $periode)
    {
        $data_count_galleries = $this->galleriesRepository->getTotalCount();
        $data_count_testimonies = $this->testimoniesRepository->getTotalCount();
        $data_count_results = $this->resultsRepository->getTotalCount();

        $data_statistik = $this->viewsRepository->get($periode);

        return ['data_count_galleries' => $data_count_galleries, 'data_count_testimonies' => $data_count_testimonies, 'data_count_results' => $data_count_results, 'data' => $data_statistik];
    }
}
