<?php

namespace App\Http\Services;

use App\Dto\UsersDto;
use Exception;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Crypt;
use App\Interfaces\Repositories\IUsersRepository;
use App\Interfaces\Services\IAuthService;

class AuthService implements IAuthService
{
    protected $usersRepository;

    public function __construct(IUsersRepository $usersRepository)
    {
        $this->usersRepository = $usersRepository;
    }

    public function authenticate(string $username, string $password)
    {
        $get_by_username = $this->usersRepository->getByKeys('username', $username);

        if (!$get_by_username || Hash::check($password, $get_by_username->username)) {
            throw new Exception('Username or password is wrong', 400);
        }

        /* 24 hours */
        $time = 86400;
        $encrypt_value = Crypt::encrypt($get_by_username->username);

        $this->usersRepository->update($get_by_username->id, array('remember_token' => $encrypt_value));

        return ['status' => 'success', 'result' => $get_by_username, 'encrypt_value' => $encrypt_value, 'time' => $time];
    }

    public function register(UsersDto $usersDto)
    {

        $usersDto->password = Hash::make($usersDto->password);
        $result = $this->usersRepository->create((array)$usersDto);

        return ['status' => 'success', 'result' => $result];
    }

    static function getCookie($time, $value)
    {
        $response = response('Set-Cookie')->cookie("login", json_encode($value), $time, "/");

        return $response;
    }
}
