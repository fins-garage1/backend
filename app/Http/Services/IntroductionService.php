<?php

namespace App\Http\Services;

use App\Dto\IntroductionDto;
use App\Interfaces\Repositories\IIntroductionRepository;
use App\Interfaces\Services\IIntroductionService;

class IntroductionService implements IIntroductionService
{
    protected $introductionRepository;

    public function __construct(IIntroductionRepository $introductionRepository)
    {
        $this->introductionRepository = $introductionRepository;
    }

    public function get()
    {
        $data = $this->introductionRepository->get();

        return ['status' => 'success', 'result' => $data];
    }

    public function create(IntroductionDto $introductionDto)
    {

        $result = $this->introductionRepository->create((array)$introductionDto);

        return ['status' => 'success', 'result' => $result];
    }

    public function update(IntroductionDto $introductionDto)
    {
        $get_data_introduction = $this->introductionRepository->get();

        if ($get_data_introduction->isEmpty()) {
            $result = $this->introductionRepository->create((array)$introductionDto);
        } else {
            $result = $this->introductionRepository->update($get_data_introduction[0]->id, (array)$introductionDto);
        }

        return ['status' => 'success', 'result' => $get_data_introduction->isEmpty() ? $result : $get_data_introduction[0]];
    }

    public function page()
    {
        $data = $this->introductionRepository->get();

        return  $data[0] ?? [];
    }
}
