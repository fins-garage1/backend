<?php

namespace App\Http\Services;

use App\Dto\TestimoniesDto;
use App\Interfaces\Repositories\ITestimoniesRepository;
use App\Interfaces\Services\ITestimoniesService;

class TestimoniesService implements ITestimoniesService
{
    protected $testimoniesRepository;

    public function __construct(ITestimoniesRepository $testimoniesRepository)
    {
        $this->testimoniesRepository = $testimoniesRepository;
    }

    public function get(int $limit, string $order_by, string $sort)
    {
        $data = $this->testimoniesRepository->get($limit, $order_by, $sort);
        $data_count = $this->testimoniesRepository->getTotalCount();

        return ['status' => 'success', 'result' => ['data' => $data, 'count' => $data_count]];
    }

    public function getById(int $id)
    {
        $data = $this->testimoniesRepository->getById($id);

        return ['status' => 'success', 'result' => ['data' => $data]];
    }


    public function create(TestimoniesDto $testimoniesDto)
    {
        $result = $this->testimoniesRepository->create((array)$testimoniesDto);

        return ['status' => 'success', 'result' => $result];
    }


    public function update(int $id, TestimoniesDto $testimoniesDto)
    {
        $result = $this->testimoniesRepository->update($id, (array)$testimoniesDto);

        return ['status' => 'success', 'result' => $result];
    }

    public function delete(int $id)
    {
        $result = $this->testimoniesRepository->delete($id);

        return ['status' => 'success', 'result' => $result];
    }

    public function page(int $limit, string $order_by, string $sort, string $search)
    {

        $data = $this->testimoniesRepository->get($limit, $order_by, $sort, $search);

        return $data;
    }
}
