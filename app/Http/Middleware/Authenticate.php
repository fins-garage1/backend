<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Auth\Middleware\Authenticate as Middleware;

class Authenticate extends Middleware
{
    /**
     * Get the path the user should be redirected to when they are not authenticated.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return string|null
     */
    // protected function redirectTo($request)
    // {
    //     if ($request->session()->get('login')) {
    //         // return route('dashboard-general-dashboard');
    //     } else {
    //         dd('User is not authenticated.');
    //     }
    // }

    public function handle($request, Closure $next, ...$guards)
    {

        if ($request->session()->get('login')) {
            return $next($request);
        } else {
            return redirect()->route('auth-login-page');
        }

        if(!$request->cookie('login')) {
            return response()->json(["message" => "Authorization Failed"], 400);
        }
    }
}
