<?php

namespace App\Requests;

class IntroductionRequest
{
    public function rules(): array
    {
        return [
            [
            'paragraph' => 'required',
            'video' => 'required'
            ],
            [
                'limit' => 'required',
                'sort' => 'required',
                'order_by' => 'required',
            ]
        ];
    }
}
