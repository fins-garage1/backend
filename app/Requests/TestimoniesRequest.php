<?php

namespace App\Requests;

class TestimoniesRequest
{
    public function rules(): array
    {
        return [
            [
            'name' => 'required',
            'position' => 'required',
            'feedback' => 'required',
            'image' => 'image|mimes:jpeg,png,jpg|max:2048'
            ],
            [
                'limit' => 'required',
                'sort' => 'required',
                'order_by' => 'required',
            ]
        ];
    }
}
