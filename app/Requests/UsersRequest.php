<?php

namespace App\Requests;

class UsersRequest
{
    public function rules(): array
    {
        return [
            [
            'username' => 'required',
            'email' => 'required',
            'phone' => 'required',
            'password' => 'required',
            'remember_token' => 'nullable'
            ],
            [
                'limit' => 'required',
                'sort' => 'required',
                'order_by' => 'required',
            ]
        ];
    }
}
