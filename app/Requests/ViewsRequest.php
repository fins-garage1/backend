<?php

namespace App\Requests;

class ViewsRequest
{
    public function rules(): array
    {
        return [
            [
                'ip_address' => 'required'
            ],
            [
                'limit' => 'required',
                'sort' => 'required',
                'order_by' => 'required',
            ]
        ];
    }
}
