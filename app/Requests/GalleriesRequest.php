<?php

namespace App\Requests;

class GalleriesRequest
{
    public function rules(): array
    {
        return [
            ['image' => 'image|mimes:jpeg,png,jpg|max:2048'],
            [
                'limit' => 'required',
                'sort' => 'required',
                'order_by' => 'required',
            ]
        ];
    }
}
