<?php

namespace App\Requests;

class ResultsRequest
{
    public function rules(): array
    {
        return [
            [
                'name' => 'required',
                'total' => 'required'
            ],
            [
                'limit' => 'required',
                'sort' => 'required',
                'order_by' => 'required',
            ]
        ];
    }
}
