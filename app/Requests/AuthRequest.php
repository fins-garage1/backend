<?php

namespace App\Requests;

class AuthRequest
{
    public function rules(): array
    {
        return [
           [
            'username' => 'required',
            'password' => 'required'
           ],
           [
            'username' => 'required',
            'email' => 'required',
            'phone' => 'required',
            'password' => 'required'
           ]
        ];
    }
}
