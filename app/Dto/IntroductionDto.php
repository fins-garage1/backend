<?php

namespace App\Dto;

class IntroductionDto
{
    public string $paragraph;
    public string $video;

    public function __construct(string $paragraph, string $video)
    {
        $this->paragraph = $paragraph;
        $this->video = $video;
    }
}
