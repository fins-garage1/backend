<?php

namespace App\Dto;

class ResultsDto
{
    public string $name;
    public string $total;

    public function __construct(string $name, string $total)
    {
        $this->name = $name;
        $this->total = $total;
    }
}
