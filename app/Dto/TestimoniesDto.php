<?php

namespace App\Dto;

class TestimoniesDto
{
    public string $image;
    public string $name;
    public string $position;
    public string $feedback;


    public function __construct(string $image, string $name, string $position, string $feedback)
    {
        $this->image = $image;
        $this->name = $name;
        $this->position = $position;
        $this->feedback = $feedback;
    }
}
