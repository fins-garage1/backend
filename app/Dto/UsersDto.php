<?php

namespace App\Dto;

class UsersDto
{
    public string $username;
    public string $email;
    public string $phone;
    public string $password;
    public string $remember_token;

    public function __construct(string $username, string $email, string $phone, string $password, string $remember_token)
    {
        $this->username = $username;
        $this->email = $email;
        $this->phone = $phone;
        $this->password = $password;
        $this->remember_token = $remember_token;
    }
}
