<?php

namespace App\Exceptions;

use Throwable;
use \Illuminate\Database\QueryException;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;

class CustomExceptionHandler extends ExceptionHandler
{
    public function render($request,  Throwable $exception)
    {
        $api = $request->input('api', false);

        if ($exception instanceof \Exception) {
            if ($api) {
                return response()->json(['message' => $exception->getMessage()], 400);
            }

            return Redirect::back()->withErrors(['message' => $exception->getMessage()]);
        }

        if ($exception instanceof QueryException) {
            $message = $exception->getMessage();
            preg_match("/for key '(.+?)'/", $message, $matches);

            if (isset($matches[1])) {
                return response()->json(['message' => 'Your ' . $matches[1] . ' is already exist'], 400);
            } else {
                return response()->json(['message' => 'Error in query: ' . $message], 400);
            }
        }

        return parent::render($request, $exception);
    }
}
