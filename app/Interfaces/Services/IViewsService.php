<?php

namespace App\Interfaces\Services;

interface IViewsService
{
    public function get(string $filter);
    public function getById(int $id);
    public function create(string $ip_address);
    public function update(int $id, string $ip_address);
    public function delete(int $id);
    public function page(string $search);
}
