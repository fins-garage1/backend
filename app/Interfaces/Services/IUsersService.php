<?php

namespace App\Interfaces\Services;

use App\Dto\UsersDto;

interface IUsersService
{
    public function get(int $limit, string $order_by, string $sort);
    public function create(UsersDto $usersDto);
    public function update(int $id, UsersDto $usersDto);
    public function delete(int $id);
}
