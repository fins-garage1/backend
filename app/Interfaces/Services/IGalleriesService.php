<?php

namespace App\Interfaces\Services;

interface IGalleriesService
{
    public function get(int $limit, string $order_by, string $sort);
    public function getById(int $id);
    public function create(string $image);
    public function update(int $id, $image);
    public function delete(int $id);
    public function page();
}
