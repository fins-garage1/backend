<?php

namespace App\Interfaces\Services;

interface IDashboardService
{
    public function page(string $periode);
}
