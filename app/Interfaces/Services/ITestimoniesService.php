<?php

namespace App\Interfaces\Services;

use App\Dto\TestimoniesDto;

interface ITestimoniesService
{
    public function get(int $limit, string $order_by, string $sort);
    public function getById(int $id);
    public function create(TestimoniesDto $testimoniesDto);
    public function update(int $id, TestimoniesDto $testimoniesDto);
    public function delete(int $id);
    public function page(int $limit, string $order_by, string $sort, string $search);
}
