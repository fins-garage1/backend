<?php

namespace App\Interfaces\Services;

use App\Dto\ResultsDto;

interface IResultsService
{
    public function get(int $limit, string $order_by, string $sort);
    public function getById(int $id);
    public function create(ResultsDto $resultsDto);
    public function update(int $id, ResultsDto $resultsDto);
    public function delete(int $id);
    public function page(string $search);
}
