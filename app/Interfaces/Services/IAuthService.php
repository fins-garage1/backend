<?php

namespace App\Interfaces\Services;

use App\Dto\UsersDto;

interface IAuthService
{
    public function authenticate(string $username, string $password);
    public function register(UsersDto $usersDto);
}
