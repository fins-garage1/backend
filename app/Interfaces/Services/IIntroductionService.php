<?php

namespace App\Interfaces\Services;

use App\Dto\IntroductionDto;

interface IIntroductionService
{
    public function get();
    public function create(IntroductionDto $introductionDto);
    public function update(IntroductionDto $introductionDto);
    public function page();
}
