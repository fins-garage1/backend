<?php
namespace App\Interfaces\Controllers;

use Illuminate\Http\Request;

interface IUsersController
{
    public function get(Request $request);
    public function create(Request $request);
    public function update(Request $request, $id);
    public function delete($id);
}
