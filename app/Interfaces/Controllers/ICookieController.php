<?php
namespace App\Interfaces\Controllers;

use Illuminate\Http\Request;

    interface ICookieController {
        public function setCookie();
        public function getCookie(Request $request);
        public function delCookie();
    }
?>
