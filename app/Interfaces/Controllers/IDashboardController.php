<?php
namespace App\Interfaces\Controllers;
use Illuminate\Http\Request;

interface IDashboardController
{
    public function page(Request $request);
}
