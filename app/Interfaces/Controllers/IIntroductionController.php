<?php
namespace App\Interfaces\Controllers;

use Illuminate\Http\Request;

interface IIntroductionController
{
    public function get(Request $request);
    public function create(Request $request);
    public function update(Request $request);
    public function page();
}
