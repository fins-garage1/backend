<?php

namespace App\Interfaces\Controllers;

use Illuminate\Http\Request;

interface IAuthController
{
    public function authenticate(Request $request);
    public function register(Request $request);
    public function logout(Request $request);
}
