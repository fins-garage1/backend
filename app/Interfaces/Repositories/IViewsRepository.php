<?php

namespace App\Interfaces\Repositories;

interface IViewsRepository
{
    public function get($filter);
    public function getById($id);
    public function getTotalCount();
    public function getByKeys($keys, $value);
    public function create($data);
    public function update($id, $data);
    public function delete($id);

}
