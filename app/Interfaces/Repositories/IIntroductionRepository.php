<?php

namespace App\Interfaces\Repositories;

interface IIntroductionRepository
{
    public function getByKeys($keys, $value);
    public function get();
    public function create($data);
    public function update($id, $data);
}
