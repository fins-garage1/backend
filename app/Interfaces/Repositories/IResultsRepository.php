<?php

namespace App\Interfaces\Repositories;

interface IResultsRepository
{
    public function get($limit = 5, $order_by = 'id', $sort = 'asc', $search = '');
    public function getById($id);
    public function getTotalCount();
    public function getByKeys($keys, $value);
    public function create($data);
    public function update($id, $data);
    public function delete($id);
}
