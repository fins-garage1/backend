<?php

namespace App\Providers;

use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Gate;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The model to policy mappings for the application.
     *
     * @var array<class-string, class-string>
     */
    protected $policies = [
        // 'App\Models\Model' => 'App\Policies\ModelPolicy',
    ];

    public function register()
    {
        // $this->app->singleton(AuthService::class, function($app) {
        //     return new AuthService($username, $password);
        // });

        // $this->app->bind(Transistor::class, function (Application $app) {
        //     return new Transistor($app->make(PodcastParser::class));
        // });
    }

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();
        //
        // $this->app->bind();
    }
}
