<?php

namespace App\Providers;

use App\Http\Controllers\AuthController;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\GalleriesController;
use App\Http\Controllers\IntroductionController;
use App\Http\Controllers\ResultsController;
use App\Http\Controllers\TestimoniesController;
use App\Http\Controllers\UsersController;
use App\Http\Controllers\ViewsController;
use App\Http\Repositories\GalleriesRepository;
use App\Http\Repositories\IntroductionRepository;
use App\Http\Repositories\ResultsRepository;
use App\Http\Repositories\TestimoniesRepository;
use App\Http\Repositories\UsersRepository;
use App\Http\Repositories\ViewsRepository;
use App\Http\Services\AuthService;
use App\Http\Services\DashboardService;
use App\Http\Services\GalleriesService;
use App\Http\Services\IntroductionService;
use App\Http\Services\ResultsService;
use App\Http\Services\TestimoniesService;
use App\Http\Services\UsersService;
use App\Http\Services\ViewsService;
use App\Interfaces\Controllers\IAuthController;
use App\Interfaces\Controllers\IDashboardController;
use App\Interfaces\Controllers\IGalleriesController as ControllersIGalleriesController;
use App\Interfaces\Controllers\IIntroductionController as ControllersIIntroductionController;
use App\Interfaces\Controllers\IResultsController as ControllersIResultsController;
use App\Interfaces\Controllers\ITestimoniesController as ControllersITestimoniesController;
use App\Interfaces\Controllers\IUsersController as ControllersIUsersController;
use App\Interfaces\Controllers\IViewsController as ControllersIViewsController;
use App\Interfaces\Repositories\IGalleriesRepository;
use App\Interfaces\Repositories\IIntroductionRepository;
use App\Interfaces\Repositories\IResultsRepository;
use App\Interfaces\Repositories\ITestimoniesRepository;
use App\Interfaces\Repositories\IUsersRepository;
use App\Interfaces\Repositories\IViewsRepository;
use App\Interfaces\Services\IAuthService;
use App\Interfaces\Services\IDashboardService;
use App\Interfaces\Services\IGalleriesService;
use App\Interfaces\Services\IIntroductionService;
use App\Interfaces\Services\IResultsService;
use App\Interfaces\Services\ITestimoniesService;
use App\Interfaces\Services\IUsersService;
use App\Interfaces\Services\IViewsService;
use Illuminate\Support\ServiceProvider;
use Illuminate\Pagination\Paginator;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        /* controller */
        $this->app->bind(IAuthController::class, AuthController::class);
        $this->app->bind(IDashboardController::class, DashboardController::class);
        $this->app->bind(ControllersIGalleriesController::class, GalleriesController::class);
        $this->app->bind(ControllersIIntroductionController::class, IntroductionController::class);
        $this->app->bind(ControllersIResultsController::class, ResultsController::class);
        $this->app->bind(ControllersITestimoniesController::class, TestimoniesController::class);
        $this->app->bind(ControllersIUsersController::class, UsersController::class);
        $this->app->bind(ControllersIViewsController::class, ViewsController::class);

        /* service */
        $this->app->bind(IAuthService::class, AuthService::class);
        $this->app->bind(IDashboardService::class, DashboardService::class);
        $this->app->bind(IGalleriesService::class, GalleriesService::class);
        $this->app->bind(IIntroductionService::class, IntroductionService::class);
        $this->app->bind(IResultsService::class, ResultsService::class);
        $this->app->bind(ITestimoniesService::class, TestimoniesService::class);
        $this->app->bind(IUsersService::class, UsersService::class);
        $this->app->bind(IViewsService::class, ViewsService::class);

        /* repository */
        $this->app->bind(IGalleriesRepository::class, GalleriesRepository::class);
        $this->app->bind(IIntroductionRepository::class, IntroductionRepository::class);
        $this->app->bind(IResultsRepository::class, ResultsRepository::class);
        $this->app->bind(ITestimoniesRepository::class, TestimoniesRepository::class);
        $this->app->bind(IUsersRepository::class, UsersRepository::class);
        $this->app->bind(IViewsRepository::class, ViewsRepository::class);
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Paginator::useBootstrap();
    }
}
