<?php

namespace App\Builder;

use App\Dto\IntroductionDto;

class IntroductionBuilder
{
    public string $paragraph = '';
    public string $video = '';

    public function setParagraph($paragraph)
    {
        $this->paragraph = $paragraph;
        return $this;
    }

    public function setVideo($video)
    {
        $this->video = $video;
        return $this;
    }

    public function build(): IntroductionDto
    {
        return new IntroductionDto(
            $this->paragraph,
            $this->video,
        );
    }
}
