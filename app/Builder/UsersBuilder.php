<?php

namespace App\Builder;

use App\Dto\UsersDto;

class UsersBuilder
{
    public string $username = '';
    public string $email = '';
    public string $phone = '';
    public string $password = '';
    public string $remember_token = '';

    public function setUsername($username)
    {
        $this->username = $username;
        return $this;
    }

    public function setEmail($email)
    {
        $this->email = $email;
        return $this;
    }

    public function setPhone($phone)
    {
        $this->phone = $phone;
        return $this;
    }

    public function setPassword($password)
    {
        $this->password = $password;
        return $this;
    }

    public function setRememberToken($remember_token)
    {
        $this->$remember_token = $remember_token;
        return $this;
    }

    public function build(): UsersDto
    {
        return new UsersDto(
            $this->username,
            $this->email,
            $this->phone,
            $this->password,
            $this->remember_token
        );
    }
}
