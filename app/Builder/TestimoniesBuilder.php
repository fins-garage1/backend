<?php

namespace App\Builder;

use App\Dto\TestimoniesDto;

class TestimoniesBuilder
{
    public string $image = '';
    public string $name = '';
    public string $position = '';
    public string $feedback = '';

    public function setImage($image)
    {
        $this->image = $image;
        return $this;
    }

    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    public function setPosition($position)
    {
        $this->position = $position;
        return $this;
    }

    public function setFeedback($feedback)
    {
        $this->feedback = $feedback;
        return $this;
    }

    public function build(): TestimoniesDto
    {
        return new TestimoniesDto(
            $this->image,
            $this->name,
            $this->position,
            $this->feedback,
        );
    }
}
