<?php

namespace App\Builder;
use App\Dto\ResultsDto;

class ResultsBuilder
{
    public string $name = '';
    public string $total = '';

    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    public function setTotal($total)
    {
        $this->total = $total;
        return $this;
    }

    public function build(): ResultsDto
    {
        return new ResultsDto(
            $this->name,
            $this->total,
        );
    }
}
