<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('username', 200)->unique();
            $table->string('email')->unique();
            $table->string('phone', 12);
            $table->text('password');
            $table->text('remember_token')->nullable();
            $table->timestamps();
        });

        Schema::create('introductions', function (Blueprint $table) {
            $table->id();
            $table->text('paragraph');
            $table->text('video');
            $table->timestamps();
        });

        Schema::create('results', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->integer('total');
            $table->timestamps();
        });

        Schema::create('galleries', function (Blueprint $table) {
            $table->id();
            $table->text('image');
            $table->timestamps();
        });

        Schema::create('testimonies', function (Blueprint $table) {
            $table->id();
            $table->string('name', 150);
            $table->string('position', 150);
            $table->string('feedback');
            $table->text('image');
            $table->timestamps();
        });

        Schema::create('views', function (Blueprint $table) {
            $table->id();
            $table->string('ip_address', 200);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
        Schema::dropIfExists('introduction');
        Schema::dropIfExists('our_results');
        Schema::dropIfExists('galleries');
        Schema::dropIfExists('testimonies');
    }
};
