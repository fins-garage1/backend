<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Users>
 */
class UsersFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
            // 'name' => fake()->name(),
            // 'email' => fake()->safeEmail(),
            // 'email_verified_at' => now(),
            // 'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password
            // 'remember_token' => Str::random(10),

            "username" => fake()->name(),
            "email" => fake()->safeEmail(),
            "phone" => "08871992701",
            "password" => '$2y$10$oE0dQ1k.1yHbWa6WvN2V1u9iikC7ZZx3KIT07mgzPD915FsjDoBQ6',
            "remember_token" => $this->faker->text(50),
            // "remember_token" => "eyJpdiI6Ilp2a3pCRkI5UTVyREV6WFlVR21zM0E9PSIsInZhbHVlIjoiZ0VKYkRhNXVQTW0xdWFra1k5emtTUT09IiwibWFjIjoiYWU0ZDgwZDVjMjk5YzM5ODQxNzczNWE1NDM5MjIzY2FjZmEyYjE0ZTBlMTIwNjRjNmIzYjI0NWI0NjY1MDZlMyIsInRhZyI6IiJ9",
            // "created_at" => "2024-01-06 02:01:32",
            // "updated_at" => "2024-01-06 02:12:22"
        ];
    }
}
