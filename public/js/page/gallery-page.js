$(document).ready(function () {

    $("#image-input").change(function (event) {
        const output = $("#image-preview");
        const fileInput = event.target;

        if (fileInput.files && fileInput.files[0]) {
            output.attr("src", URL.createObjectURL(fileInput.files[0]));
        }
    });

    $(".button-editOpen-galeri").click(function () {
        const id = $(this).data("id");
        const URL = BASE_URL + "/core/gallery/getById" + "/" + id + "?api=yes";

        $.get(URL, function (data) {
            const imagePath = assetUrl + "/" + data.result.data.image;

            $("#image-preview").attr("src", imagePath);
            $("#image-input").attr("value", imagePath);

            $("#modal-title-testimonies").text("Edit data testimoni");

            $("#modal-title-galeri").text("Edit data galeri");
        });

        $(".id-galeri").val(id);
        $(".button-modal-galeri").addClass("button-edit-galeri");
        $(".button-modal-galeri").text("Simpan perubahan");
        $("#modal-form-galeri").attr("method", "POST");

        $("#image-input").prop("required", false);
        $("#modal-form-galeri").attr(
            "action",
            BASE_URL + "/core/gallery/update/" + id
        );
    });

    $(".button-addOpen-galeri").click(function () {

        $(".image-galeri").val("");

        $("#modal-title-galeri").text("Tambah data galeri");
        $(".button-modal-galeri").addClass("button-add-galeri");
        $(".button-modal-galeri").text("Tambah data galeri");
        $("#modal-form-galeri").attr("method", "POST");
        $("#modal-form-galeri").attr(
            "action",
            BASE_URL + "/core/gallery/create"
        );

        $("#image-input").prop("required", true);
        $("#image-preview").attr("src", "");
    });
});
