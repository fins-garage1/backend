$(document).ready(function () {
    $("#image-input").change(function (event) {
        const output = $("#image-preview");
        const fileInput = event.target;

        if (fileInput.files && fileInput.files[0]) {
            output.attr("src", URL.createObjectURL(fileInput.files[0]));
        }
    });

    $(".button-editOpen-testimonies").click(function () {
        const id = $(this).data("id");
        const URL =
            BASE_URL + "/core/testimoni/getById" + "/" + id + "?api=yes";

        $.get(URL, function (data) {
            const imagePath = assetUrl + "/" + data.result.data.image;

            $(".name-testimonies").val(data.result.data.name);
            $(".position-testimonies").val(data.result.data.position);
            $(".feedback-testimonies").val(data.result.data.feedback);
            $("#image-preview").attr("src", imagePath);
            $("#image-input").attr("value", imagePath);

            $("#modal-title-testimonies").text("Edit data testimoni");
        });

        $(".id-testimonies").val(id);
        $(".button-modal-testimonies").addClass("button-edit-testimonies");
        $(".button-modal-testimonies").text("Simpan perubahan");
        $("#modal-form-testimonies").attr("method", "POST");

        $("#image-input").prop("required", false);
        $("#modal-form-testimonies").attr(
            "action",
            BASE_URL + "/core/testimoni/update/" + id
        );
    });

    $(".button-addOpen-testimonies").click(function () {
        $(".name-testimonies").val("");

        $(".position-testimonies").val("");
        $(".feedback-testimonies").val("");
        $(".image-testimonies").val("");

        $("#modal-title-testimonies").text("Tambah data testimoni");
        $(".button-modal-testimonies").addClass("button-add-testimonies");
        $(".button-modal-testimonies").text("Tambah data");
        $("#modal-form-testimonies").attr("method", "POST");
        $("#modal-form-testimonies").attr(
            "action",
            BASE_URL + "/core/testimoni/create"
        );

        $("#image-input").prop("required", true);
        $("#image-preview").attr("src", "");
    });

    $('input[name="image"]').change(function (e) {
        let reader = new FileReader();
        reader.onload = function () {
            $("#preview-image-testimonies").attr("src", reader.result);
        };
        reader.readAsDataURL(e.target.files[0]);
    });
});
