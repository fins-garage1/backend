$(document).ready(function () {
    $(".button-editOpen-results").click(function () {
        const id = $(this).data("id");
        const URL = BASE_URL + "/core/result/getById" + "/" + id + "?api=yes";

        $.get(URL, function (data) {
            $(".name-results").val(data.result.data.name);
            $(".total-results").val(data.result.data.total);

            $("#modal-title-results").text("Edit data hasil kami");
        });

        $(".id-results").val(id);
        $(".button-modal-results").addClass("button-edit-results");
        $(".button-modal-results").text("Simpan perubahan");
        $("#modal-form-results").attr("method", "POST");
        $("#modal-form-results").attr(
            "action",
            BASE_URL + "/core/result/update/" + id
        );
    });

    $(".button-addOpen-results").click(function () {
        $(".name-results").val("");
        $(".total-results").val("");
        $("#modal-title-results").text("Tambah data hasil kami");
        $(".button-modal-results").addClass("button-add-results");
        $(".button-modal-results").text("Tambah data");
        $("#modal-form-results").attr("method", "POST");
        $("#modal-form-results").attr(
            "action",
            BASE_URL + "/core/result/create"
        );
    });
});
