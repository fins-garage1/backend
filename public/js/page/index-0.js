"use strict";

var statistics_chart = document.getElementById("myChart").getContext("2d");

window.periodeValue = window.periodeValue ? window.periodeValue : "day";

function convertDay(day) {
    const listDay = [
        "Minggu",
        "Senin",
        "Selasa",
        "Rabu",
        "Kamis",
        "Jumat",
        "Sabtu",
    ];

    return listDay[day];
}

function convertMonth(month) {
    const listMonth = [
        "Januari",
        "Februari",
        "Maret",
        "April",
        "Mei",
        "Juni",
        "Juli",
        "Agustus",
        "September",
        "Oktober",
        "November",
        "December",
    ];

    return listMonth[month - 1];
}

function dataToBackward(data_label, data_total, current_date, data, number) {
    for (let i = 0; i < number; i++) {
        let current_date_backward =
            window.periodeValue == "month" ? current_date + 1 : current_date;
        current_date_backward = current_date_backward - i;

        const find_data = data.find(
            (item) => item[`${window.periodeValue}`] == current_date_backward
        );

        if (window.periodeValue == "year") {
            data_label.push(
                find_data ? find_data.year : new Date().getFullYear() - i
            );
        } else if (window.periodeValue == "month") {
            data_label.push(
                convertMonth(
                    find_data
                        ? find_data.month
                        : (new Date().getMonth() - i + 12) % 12
                )
            );
        } else {
            data_label.push(
                convertDay(
                    find_data
                        ? new Date(find_data.created_at).getDay()
                        : (new Date().getDay() - i + 7) % 7
                )
            );
        }

        data_total.push(find_data ? find_data.total : 0);
    }
}

function dataSet(data) {
    let data_labels = [];
    let data_total = [];

    let current_date;

    switch (window.periodeValue) {
        case "month":
            current_date = new Date().getMonth();
            break;
        case "year":
            current_date = new Date().getFullYear();
            break;
        default:
            current_date = new Date().getDate();
            break;
    }

    if (window.periodeValue == "month") {
        dataToBackward(data_labels, data_total, current_date, data, 12);
    } else if (window.periodeValue == "year") {
        dataToBackward(data_labels, data_total, current_date, data, 7);
    } else {
        dataToBackward(data_labels, data_total, current_date, data, 7);
    }

    return [data_total.reverse(), data_labels.reverse()];
}

const declareEvent = dataSet(window.data_views);

var myChart = new Chart(statistics_chart, {
    type: "line",
    data: {
        labels: declareEvent[1],
        datasets: [
            {
                label: "Total",
                data: declareEvent[0],
                borderWidth: 5,
                borderColor: "#6777ef",
                backgroundColor: "transparent",
                pointBackgroundColor: "#fff",
                pointBorderColor: "#6777ef",
                pointRadius: 4,
            },
        ],
    },
    options: {
        legend: {
            display: false,
        },
        scales: {
            yAxes: [
                {
                    gridLines: {
                        display: false,
                        drawBorder: false,
                    },
                    ticks: {
                        stepSize: 150,
                    },
                },
            ],
            xAxes: [
                {
                    gridLines: {
                        color: "#fbfbfb",
                        lineWidth: 2,
                    },
                },
            ],
        },
    },
});

$("#visitorMap").vectorMap({
    map: "world_en",
    backgroundColor: "#ffffff",
    borderColor: "#f2f2f2",
    borderOpacity: 0.8,
    borderWidth: 1,
    hoverColor: "#000",
    hoverOpacity: 0.8,
    color: "#ddd",
    normalizeFunction: "linear",
    selectedRegions: false,
    showTooltip: true,
    pins: {
        id: '<div class="jqvmap-circle"></div>',
        my: '<div class="jqvmap-circle"></div>',
        th: '<div class="jqvmap-circle"></div>',
        sy: '<div class="jqvmap-circle"></div>',
        eg: '<div class="jqvmap-circle"></div>',
        ae: '<div class="jqvmap-circle"></div>',
        nz: '<div class="jqvmap-circle"></div>',
        tl: '<div class="jqvmap-circle"></div>',
        ng: '<div class="jqvmap-circle"></div>',
        si: '<div class="jqvmap-circle"></div>',
        pa: '<div class="jqvmap-circle"></div>',
        au: '<div class="jqvmap-circle"></div>',
        ca: '<div class="jqvmap-circle"></div>',
        tr: '<div class="jqvmap-circle"></div>',
    },
});

// weather
getWeather();
setInterval(getWeather, 600000);

function getWeather() {
    $.simpleWeather({
        location: "Bogor, Indonesia",
        unit: "c",
        success: function (weather) {
            var html = "";
            html += '<div class="weather">';
            html +=
                '<div class="weather-icon text-primary"><span class="wi wi-yahoo-' +
                weather.code +
                '"></span></div>';
            html += '<div class="weather-desc">';
            html +=
                "<h4>" + weather.temp + "&deg;" + weather.units.temp + "</h4>";
            html += '<div class="weather-text">' + weather.currently + "</div>";
            html += "<ul><li>" + weather.city + ", " + weather.region + "</li>";
            html +=
                '<li> <i class="wi wi-strong-wind"></i> ' +
                weather.wind.speed +
                " " +
                weather.units.speed +
                "</li></ul>";
            html += "</div>";
            html += "</div>";

            $("#myWeather").html(html);
        },
        error: function (error) {
            $("#myWeather").html(
                '<div class="alert alert-danger">' + error + "</div>"
            );
        },
    });
}
