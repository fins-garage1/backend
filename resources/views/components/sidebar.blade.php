<div class="main-sidebar sidebar-style-2">
    <aside id="sidebar-wrapper">
        <div class="sidebar-brand">
            <a href="{{ url('dashboard-general-dashboard') }}">FINSGARAGE</a>
        </div>
        <div class="sidebar-brand sidebar-brand-sm">
            <a href="index.html">FG</a>
        </div>
        <ul class="sidebar-menu">

            <li class="dashboard-menu {{ $type_menu === 'dashboard' ? 'active' : '' }}">
                <a class="nav-link" href="{{ url('dashboard-general-dashboard') }}"><i class="fas fa-tachometer-alt"></i>
                    <span>Dashboard</span></a>
            </li>
            <li class="perkenalan-menu {{ $type_menu === 'introduction' ? 'active' : '' }}">
                <a class="nav-link" href="{{ url('perkenalan-page') }}"><i class="far fa-note-sticky"></i>
                    <span>Perkenalan</span></a>
            </li>
            <li class="hasil-kami-menu {{ $type_menu === 'result' ? 'active' : '' }}">
                <a class="nav-link" href="{{ url('hasil-kami-page') }}"><i class="far fa-circle-check"></i> <span>Hasil
                        Kami</span></a>
            </li>
            <li class="testimoni-menu {{ $type_menu === 'testimoni' ? 'active' : '' }}">
                <a class="nav-link" href="{{ url('testimoni-page') }}"><i class="far fa-user"></i>
                    <span>Testimoni</span></a>
            </li>
            <li class="galeri-menu {{ $type_menu === 'gallery' ? 'active' : '' }}">
                <a class="nav-link" href="{{ url('galeri-page') }}"><i class="far fa-image"></i>
                    <span>Galeri</span></a>
            </li>
    </aside>
</div>
