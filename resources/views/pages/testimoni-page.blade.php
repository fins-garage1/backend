@extends('layouts.app')

@section('title', 'Halaman Testimoni')

@push('style')
    <!-- CSS Libraries -->
    <!-- CSS Libraries -->
    <link rel="stylesheet" href="{{ asset('css/pages/testimoni-page.css') }}">
@endpush

@section('main')<div class="main-content">
        <section class="section">
            <div class="section-header">
                <h1>Testimoni</h1>
            </div>

            <div class="section-body">
                <div class="card">
                    <div class="card-header">
                        <h4 class="text-dark">Urutan tabel Testimoni</h4>

                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <div class="d-flex justify-content-between">
                                <form method="GET" action="{{ url('testimoni-page') }}">
                                    @csrf

                                    <div class="d-flex">
                                        <input placeholder="Cari di sini" name="search" type="search"
                                            class="form-control">
                                        <button class="btn btn-dark" type="submit">Cari</button>
                                    </div>
                                </form>

                                <button class="button-addOpen-testimonies btn btn-dark" data-toggle="modal"
                                    data-target="#testimoniModal">Tambah Testimoni
                                </button>
                            </div>
                            <table class="table-bordered table-md table mt-3">
                                <tr>
                                    <th>No</th>
                                    <th>Nama</th>
                                    <th>Status / Peran</th>
                                    <th>Pesan</th>
                                    <th>Gambar</th>
                                    <th>Di Buat</th>
                                    <th>Aksi</th>
                                </tr>
                                @foreach ($result as $item)
                                    <tr>
                                        <th scope="row">{{ $loop->iteration }}</th>
                                        <td>{{ $item['name'] }}</td>
                                        <td>{{ $item['position'] }}</td>
                                        <td>{{ $item['feedback'] }}</td>
                                        <td class="p-2">
                                            <img width="150" height="150"
                                                src="{{ asset('storage/testimonies/' . $item['image']) }}"
                                                alt="{{ 'Picture ' . $loop->iteration }}" />
                                        </td>
                                        <td>{{ $item['created_at'] }}</td>
                                        <td class="d-flex">
                                            <button data-id="{{ $item['id'] }}"
                                                class="m-1 button-editOpen-testimonies btn btn-dark" data-toggle="modal"
                                                data-target="#testimoniModal">Edit</button>

                                            <form class="m-1" id="delete-form-testimonies" method="GET" action="">
                                                @csrf
                                                <button class="btn btn-danger"
                                                    data-confirm="Yakin?|Apakah kamu tetap lanjutkan?"
                                                    data-confirm-yes="submitForm('{{ route('testimoni-delete', ['id' => $item['id']]) }}')">Hapus</button>
                                            </form>

                                        </td>
                                    </tr>
                                @endforeach
                            </table>
                        </div>
                    </div>
                    <div class="card-footer text-right">
                        <nav class="d-flex justify-content-end">
                            {{ $result->links('pagination::bootstrap-4') }}
                        </nav>
                    </div>
                </div>
            </div>
    </div>

    <div class="modal fade" tabindex="-1" role="dialog" id="testimoniModal">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 id="modal-title-testimonies">Edit Testimoni</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form id="modal-form-testimonies" method="POST" enctype="multipart/form-data">
                        @csrf

                        <div class="form-group">
                            <label>Nama</label>
                            <input name="name" type="text" placeholder="Isi nama"
                                class="name-testimonies form-control" required>
                        </div>
                        <div class="form-group">
                            <label>Status / Peran</label>
                            <input name="position" type="text" placeholder="Isi status / peran"
                                class="position-testimonies form-control" required>
                        </div>
                        <div class="form-group">
                            <label>Pesan</label>
                            <textarea name="feedback" class="feedback-testimonies form-control" placeholder="Isi pesan" data-height="100" required></textarea>
                        </div>
                        <div class="form-group d-flex flex-column">
                            <label>File Gambar</label>
                            <img id="image-preview" src="" alt="Image preview"
                                style="max-width: 100%; max-height: 200px;">

                            <input id="image-input" value="" name="image" type="file" class="form-control mt-3"
                                accept="image/*">
                        </div>

                        <div class="modal-footer bg-whitesmoke br">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                            <button type="submit" class="btn btn-dark button-modal-testimonies">Tambah data</button>
                            <input hidden class="id-testimonies" value="" />
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    </section>
    </div>
@endsection

@push('scripts')
    <!-- JS Libraies -->
    <script src="{{ asset('library/prismjs/prism.js') }}"></script>

    <!-- Page Specific JS File -->
    <script src="{{ asset('js/page/bootstrap-modal.js') }}"></script>
    <script src="{{ asset('js/page/testimoni-page.js') }}"></script>
    <script>
        const assetUrl = "{{ asset('storage/testimonies/') }}";

        function submitForm(deleteRoute) {
            document.getElementById('delete-form-testimonies').action = deleteRoute;
            document.getElementById('delete-form-testimonies').submit();
        }
    </script>
@endpush
