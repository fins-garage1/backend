@extends('layouts.app')

@section('title', 'Halaman Hasil Kami')

@push('style')
    <!-- CSS Libraries -->
    <link rel="stylesheet" href="{{ asset('css/pages/our-results-page.css') }}">
@endpush

@section('main')<div class="main-content">
        <section class="section">
            <div class="section-header">
                <h1>Hasil Kami</h1>
            </div>

            <div class="section-body">
                <div class="card">
                    <div class="card-header">
                        <h4 class="text-dark">Urutan tabel Hasil Kami</h4>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <div class="d-flex justify-content-between">
                                <form method="GET" action="{{ url('hasil-kami-page') }}">
                                    @csrf

                                    <div class="d-flex">
                                        <input placeholder="Cari di sini" name="search" type="search"
                                            class="form-control">
                                        <button class="btn btn-dark" type="submit">Cari</button>
                                    </div>
                                </form>

                                <button class="button-addOpen-results btn btn-dark" data-toggle="modal"
                                    data-target="#hasilKamiModal">Tambah Hasil
                                    Kami</button>
                            </div>

                            <table class="table-bordered table-md table mt-3">
                                <tr>
                                    <th>No</th>
                                    <th>Nama</th>
                                    <th>Total</th>
                                    <th>Di Buat</th>
                                    <th>Aksi</th>
                                </tr>
                                @foreach ($result as $item)
                                    <tr>
                                        <th scope="row">{{ $loop->iteration }}</th>
                                        <td class="p-2">{{ $item['name'] }}</td>
                                        <td>{{ $item['total'] }}</td>
                                        <td>{{ $item['created_at'] }}</td>
                                        <td class="d-flex">
                                            <button data-id="{{ $item['id'] }}"
                                                class="m-1 button-editOpen-results btn btn-dark" data-toggle="modal"
                                                data-target="#hasilKamiModal">Edit</button>

                                            <form class="m-1" id="delete-form-results" method="GET" action="">
                                                @csrf
                                                <button class="btn btn-danger"
                                                    data-confirm="Yakin?|Apakah kamu tetap lanjutkan?"
                                                    data-confirm-yes="submitForm('{{ route('result-delete', ['id' => $item['id']]) }}')">Hapus</button>
                                            </form>

                                        </td>
                                    </tr>
                                @endforeach
                            </table>
                        </div>
                    </div>

                    <div class="card-footer text-right">
                        <nav class="d-flex justify-content-end">
                            {{ $result->links('pagination::bootstrap-4') }}
                        </nav>
                    </div>
                </div>
            </div>
    </div>
    </div>

    <div class="modal fade" tabindex="-1" role="dialog" id="hasilKamiModal">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 id="modal-title-results" class="modal-title">Add data hasil kami</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form id="modal-form-results" method="POST">
                    @csrf
                    <div class="modal-body">
                        <div class="form-group">
                            <label>Nama</label>
                            <input name="name" type="text" placeholder="Isi nama" class="name-results form-control"
                                value="" required>
                        </div>
                        <div class="form-group">
                            <label>Total</label>
                            <input name="total" type="number" placeholder="Isi total" class="total-results form-control"
                                value="" required>
                        </div>
                    </div>
                    <div class="modal-footer bg-whitesmoke br">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                        <button type="submit" class="button-modal-results btn btn-dark">Tambah data</button>
                        <input hidden class="id-results" value="" />
                    </div>
                </form>
            </div>
        </div>
    </div>
    </section>
    </div>
@endsection

@push('scripts')
    <!-- JS Libraies -->
    <script>
        function submitForm(deleteRoute) {
            document.getElementById('delete-form-results').action = deleteRoute;
            document.getElementById('delete-form-results').submit();
        }
    </script>
    <script src="{{ asset('library/prismjs/prism.js') }}"></script>

    <!-- Page Specific JS File -->
    <script src="{{ asset('js/page/bootstrap-modal.js') }}"></script>
    <script src="{{ asset('js/page/our-results-page.js') }}"></script>
@endpush
