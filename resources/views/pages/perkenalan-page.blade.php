@extends('layouts.app')

@section('title', 'Halaman Perkenalan')

@push('style')
    <!-- CSS Libraries -->
    <!-- CSS Libraries -->
    <link rel="stylesheet" href="{{ asset('css/pages/introduction-page.css') }}">
@endpush

@section('main')<div class="main-content">
        <section class="section">
            <div class="section-header">
                <h1>Perkenalan</h1>
            </div>

            <div class="section-body">
                <div class="card">
                    <div class="card-header">
                        <h4 class="text-dark">Form Perkenalan</h4>
                    </div>
                    <div class="card-body">
                        <form method="POST" action="{{ route('introduction-update') }}">
                            @csrf

                            <div class="form-group">
                                <label>Video URL (Youtube)</label>
                                <input name="video" type="text" placeholder="Isi nama" class="form-control"
                                    value="{{ $result['video'] ?? '' }}" required>
                            </div>
                            <div class="form-group">
                                <label>Pesan</label>

                                <textarea name="paragraph" class="form-control" placeholder="Isi pesan" data-height="100" required>{{ $result['paragraph'] ?? '' }}</textarea>

                            </div>
                            <div class="d-flex justify-content-end">
                                <button type="submit" class="btn btn-dark">Simpan Perubahan</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection

@push('scripts')
    <!-- JS Libraies -->
    <script src="{{ asset('library/prismjs/prism.js') }}"></script>

    <!-- Page Specific JS File -->
    <script src="{{ asset('js/page/bootstrap-modal.js') }}"></script>
@endpush
