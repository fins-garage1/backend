@extends('layouts.app')

@section('title', 'Halaman Galeri')

@push('style')
    <!-- CSS Libraries -->
    <link rel="stylesheet" href="{{ asset('css/pages/gallery-page.css') }}">
@endpush

@section('main')<div class="main-content">
        <section class="section">
            <div class="section-header">
                <h1>Galeri</h1>
            </div>

            <div class="section-body">
                <div class="card">
                    <div class="card-header">
                        <h4 class="text-dark">Urutan tabel Galeri</h4>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <div class="d-flex justify-content-end">
                                <button class="button-addOpen-galeri btn btn-dark" data-toggle="modal"
                                    data-target="#galeriModal">Tambah
                                    Galeri</button>
                            </div>
                            <table class="table-bordered table-md table mt-3">
                                <tr>
                                    <th>No</th>
                                    <th>Gambar</th>
                                    <th>Di Buat</th>
                                    <th>Aksi</th>
                                </tr>
                                @foreach ($result as $item)
                                    <tr>
                                        <th scope="row">{{ $loop->iteration }}</th>
                                        <td class="p-2">
                                            <img width="150" height="150"
                                                src="{{ asset('storage/galleries/' . $item['image']) }}"
                                                alt="{{ 'Picture ' . $loop->iteration }}" />
                                        </td>
                                        <td>{{ $item['created_at'] }}</td>
                                        <td class="d-flex">
                                            <button class="button-editOpen-galeri btn btn-dark"
                                                data-id="{{ $item['id'] }}" data-toggle="modal"
                                                data-target="#galeriModal">Edit</button>

                                            <form class="m-1" id="delete-form-galeri" method="GET" action="">
                                                @csrf
                                                <button class="btn btn-danger"
                                                    data-confirm="Yakin?|Apakah kamu tetap lanjutkan?"
                                                    data-confirm-yes="submitForm('{{ route('gallery-delete', ['id' => $item['id']]) }}')">Hapus</button>
                                            </form>
                                        </td>
                                    </tr>
                                @endforeach
                            </table>
                        </div>
                    </div>
                    <div class="card-footer text-right">
                        <nav class="d-flex justify-content-end">
                            {{ $result->links('pagination::bootstrap-4') }}

                        </nav>
                    </div>
                </div>
            </div>
    </div>
    </div>

    <div class="modal fade" tabindex="-1" role="dialog" id="galeriModal">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 id="modal-title-galeri">Edit Galeri</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form id="modal-form-galeri" method="POST" enctype="multipart/form-data">
                        @csrf

                        <div class="form-group mt-3 d-flex flex-column">
                            <label>File Gambar</label>

                            <img id="image-preview" src="" alt="Image preview"
                                style="max-width: 100%; max-height: 200px;">

                            <input id="image-input" name="image" type="file" value=""
                                class="image-galeri form-control mt-3" accept="image/*">
                        </div>
                        <div class="modal-footer bg-whitesmoke br">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                            <button type="submit" class="btn btn-dark button-modal-galeri">Simpan Perubahan</button>
                            <input hidden class="id-galeri" value="" />
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    </section>
    </div>
@endsection

@push('scripts')
    <!-- JS Libraies -->
    <script src="{{ asset('library/prismjs/prism.js') }}"></script>

    <!-- Page Specific JS File -->
    <script src="{{ asset('js/page/bootstrap-modal.js') }}"></script>
    <script src="{{ asset('js/page/gallery-page.js') }}"></script>
    <script>
        const assetUrl = "{{ asset('storage/galleries/') }}";

        function submitForm(deleteRoute) {
            document.getElementById('delete-form-galeri').action = deleteRoute;
            document.getElementById('delete-form-galeri').submit();
        }
    </script>
@endpush
