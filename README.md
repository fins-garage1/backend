## Quick start

Several quick start options are available:

-   Clone the repo: `git clone https://gitlab.com/fins-garage1/backend`
-   Run `cd` to the newly created `/finsgarage` directory
-   Run `composer install` command
-   Run `npm install` command
-   Run `npm run dev` command
-   Run `cp .env.example .env` command
-   Run `php artisan key:generate` command
-   Run `php artisan serve` command
-   Done
